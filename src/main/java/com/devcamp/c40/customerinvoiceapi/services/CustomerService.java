package com.devcamp.c40.customerinvoiceapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.c40.customerinvoiceapi.models.Customer;

@Service
public class CustomerService {

    Customer customer1 = new Customer(1, "Nguyen Van A", 10);
    Customer customer2 = new Customer(2, "Nguyen Van B", 20);
    Customer customer3 = new Customer(3, "Nguyen Van C", 30);

    Customer customer4 = new Customer(4, "Nguyen Van A", 40);
    Customer customer5 = new Customer(5, "Nguyen Van B", 50);
    Customer customer6 = new Customer(6, "Nguyen Van C", 60);

    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> allCustomer = new ArrayList<Customer>();

        allCustomer.add(customer1);
        allCustomer.add(customer2);
        allCustomer.add(customer3);

        allCustomer.add(customer4);
        allCustomer.add(customer5);
        allCustomer.add(customer6);

        return allCustomer;
    }
}
