package com.devcamp.c40.customerinvoiceapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.c40.customerinvoiceapi.models.Customer;
import com.devcamp.c40.customerinvoiceapi.models.Invoice;

@Service
public class InvoiceService {
    @Autowired
    CustomerService customerService;

    public ArrayList<Invoice> getAllInvoices() {
        ArrayList<Invoice> allInvoices = new ArrayList<Invoice>();

        Customer customer1 = customerService.customer1;
        Customer customer2 = customerService.customer2;
        Customer customer3 = customerService.customer3;

        Customer customer4 = customerService.customer4;
        Customer customer5 = customerService.customer5;
        Customer customer6 = customerService.customer6;

        Invoice invoice1 = new Invoice(1, customer1, 100000);
        Invoice invoice2 = new Invoice(2, customer2, 200000);
        Invoice invoice3 = new Invoice(3, customer3, 300000);

        Invoice invoice4 = new Invoice(4, customer4, 100000);
        Invoice invoice5 = new Invoice(5, customer5, 200000);
        Invoice invoice6 = new Invoice(6, customer6, 300000);

        allInvoices.add(invoice1);
        allInvoices.add(invoice2);
        allInvoices.add(invoice3);

        allInvoices.add(invoice4);
        allInvoices.add(invoice5);
        allInvoices.add(invoice6);

        return allInvoices;
    }

}
