package com.devcamp.c40.customerinvoiceapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.c40.customerinvoiceapi.models.Invoice;
import com.devcamp.c40.customerinvoiceapi.services.InvoiceService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class InvoiceController {
    @Autowired
    InvoiceService invoiceService;

    @GetMapping("/invoices")
    public ArrayList<Invoice> getAllInvoices(){
        ArrayList<Invoice> allInvoice = invoiceService.getAllInvoices();

        return allInvoice;
    }

    @GetMapping("/invoices/{invoiceId}")
    public Invoice getInvoiceById(@PathVariable int invoiceId) {
        ArrayList<Invoice> allInvoice = invoiceService.getAllInvoices();

        if (invoiceId >= 0 && invoiceId < allInvoice.size() -1) {
            return allInvoice.get(invoiceId);
        } else {
            return null;
        }
    }

}
